# Note(s)

Note(s) est un outil web d'écriture, de partage et d'archive. 
Le contenu d'un framapad dicte une structure HTML. 
Il est possible de parcourir les notes dans un flux continu et par l'intermédiaire du tri. 

L'écriture dans un pad doit respecter la [syntaxe Markdown](https://guides.github.com/features/mastering-markdown/). L'export texte (txt) du pad est transformé en HTML avec le parser PHP [Parsedown](https://github.com/erusev/parsedown) et son extension Parsedown Extra.
Le tri utilise la libraire JavaScript [Isotope](https://github.com/metafizzy/isotope).

Note(s) a été écrit dans le cadre d'un Master en Design Graphique à l'ÉSAD•Valence en 2016, sur une base écrite par Émile Greis.

Le code source de Note(s) est plein d'approximations et de mauvaises pratiques. Il ne sera pas maintenu. Une version entièrement réécrite est en préparation.

## Installation

Note(s) fonctionne sur un serveur PHP. Ce dépôt est fonctionnel car il intègre un lien vers un pad de démonstration.

## Configuration

Le fichier `config/config.yaml` permet de changer le framapad à intégrer. Il suffit de coller le lien de partage du pad (sans la spécification de l'export txt).

## Utilisation

L'écriture dans le pad doit respecter la [syntaxe Markdown](https://guides.github.com/features/mastering-markdown/).
Il est impératif que chaque note commence par un `h1` (#) et finisse par un `h4` (####). C'est nécessaire pour que toutes les notes soient divisisées afin de permettre le tri.

## Licence
Le code source de Note(s) est publié sous licence [AGPL-3.0.](https://www.gnu.org/licenses/agpl-3.0.en.html).