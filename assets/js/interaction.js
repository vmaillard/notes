
// Au click sur, "toutes les notices", on referme les trieurs
function fermeture () {
  var ouvert = document.getElementsByClassName("ouvert");
  if(ouvert.length) {
    $(".ouvert:eq(0)").slideUp(250);
    ouvert[0].classList.toggle("ouvert");
  }
}

function enScène () {
  var ouvert = document.getElementsByClassName("ouvert");
  var testOuvert = this.nextElementSibling.classList.contains("ouvert");

  if (testOuvert) {
  $(this).next().slideUp(250);
  this.getElementsByTagName('span')[0].classList.toggle("survol");
  this.nextElementSibling.classList.toggle("ouvert");
  } else {
  if(ouvert.length) {
    $(".ouvert:eq(0)").slideUp(250);
    ouvert[0].classList.toggle("ouvert");
  }
  $(this).next().slideDown(250);
  this.nextElementSibling.classList.toggle("ouvert");
  this.getElementsByTagName('span')[0].classList.toggle("survol");
  }
}

function survol () {
  var testOuvert = this.nextElementSibling.classList.contains("ouvert");
  if (testOuvert) {
  this.getElementsByTagName('span')[0].classList.toggle("survol");
  }
}

function horsvol () {
  var testSurvol = this.getElementsByTagName('span')[0].classList.contains("survol");
  if (testSurvol) {
  this.getElementsByTagName('span')[0].classList.toggle("survol");
  }
}


function montreMoi () {
  // Si on clique sur un lien dans div#content,
  // on déplie le trieur correspondant
  if (this.parentNode.nodeName !== "UL") {
  var ouvert = document.getElementsByClassName("ouvert");
  var value = this.parentNode.nodeName;
  var pCorrespondant = document.getElementById(value);
  var ulCorrespondant = pCorrespondant.nextElementSibling;  
  var testOuvert = ulCorrespondant.classList.contains("ouvert");
  
  if (testOuvert) {
  } else {
  if(ouvert.length) {
  $(".ouvert:eq(0)").slideUp(250);
  ouvert[0].classList.toggle("ouvert"); 
  }
  ulCorrespondant.classList.toggle("ouvert");
  $("#" + value).next().slideDown(250);
  }
  }

  // Au clic sur un bouton déja sélectionné, on referme les trieurs
  if (this.classList.contains("selected")) {
  var ouvert = document.getElementsByClassName("ouvert");
  $(".ouvert:eq(0)").slideUp(250);
  ouvert[0].classList.toggle("ouvert");
  }
}





// Écrire
(function($){
    $.fn.extend({ 
        autoresize: function() {
            return this.each(function() {
                hiddenDiv = $(document.createElement('div'));
                content = null;
                $(this).addClass('txtstuff');
                hiddenDiv.addClass('hiddendiv common');
                $('body').append(hiddenDiv);
                    content = $(this).val();
                    content = content.replace(/\n/g, '<br>');
                    hiddenDiv.html(content + "<br class='lbr'>");
                    $(this).css('height', hiddenDiv.height());
                $(this).on('keyup', function() {
                    content = $(this).val();
                    content = content.replace(/\n/g, '<br>');
                    hiddenDiv.html(content + "<br class='lbr'>");
                    $(this).css('height', hiddenDiv.height());
                });
            });
        }
    });
})(jQuery);

//apply the autoresize feature
$('.js-copytextarea:eq(0)').autoresize();


var copyTextareaBtn = document.querySelector('.js-textareacopybtn');
if (copyTextareaBtn != undefined) {

copyTextareaBtn.addEventListener('click', function(event) {
  var copyTextarea = document.querySelector('.js-copytextarea');
  copyTextarea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }
});
}



var recherche = document.getElementsByClassName("box");

for (var i = 0; i < recherche.length; i++) {
  recherche[i].addEventListener('input', rechercher);
}

function rechercher () {
  var selector = this.parentNode.getElementsByClassName("bouton");
  var matcher = new RegExp(this.value, "gi");
  for (var i=0;i<selector.length;i++) {
    if (matcher.test(selector[i].innerHTML)) {
    selector[i].style.display="inline-block";
    } else {
    selector[i].style.display="none";
    }
      
  }
}





var tagsExistants = document.getElementsByClassName("tagsExistants")[0];
var h3 = document.getElementsByTagName("h3");
var tableauH3 = [];

// Si on est sur écrire.php, on met en place les mots-clés déja existants
if (argumentOntologique(tagsExistants)) {
  creationTag(tableauH3, h3);
  // On supprime tous les enfants
  var fc = tagsExistants.firstChild;
  while( fc ) {
  tagsExistants.removeChild(fc);
  fc = tagsExistants.firstChild;
  }
  // On met uniquement les mots clés
  for (var i in tableauH3) {
  tagsExistants.appendChild(tableauH3[i]);
  }

}


// vérifier l'existence
function argumentOntologique(element) {
  if (element != undefined) return true;
}

