function getHashFilter() {
  var hash = location.hash;
  // get filter=filterName
  var matches = location.hash.match( /filter=([^&]+)/i );
  var hashFilter = matches && matches[1];
  return hashFilter && decodeURIComponent( hashFilter );
}

$( function() {

  var $container = $('#content');
  // bind filter button click
  var $filters = $('.filter a').click(function(){
    $this = $( this );
    var filterAttr = $( this ).attr('data-filter-value');
    // set filter in hash
    location.hash = 'filter=' + encodeURIComponent( filterAttr );

    if ( $this.hasClass('selected') ) {
    var $optionSet = $('.option-set');
    $optionSet.find('.selected').removeClass('selected');    
    $container.isotope({
      filter: '*',
      masonry: {
        gutter: 20
      }
    }); 
    location.hash = 'filter=' + encodeURIComponent( '*' );
    }

  });

  var isIsotopeInit = false;

  function onHashchange() {
    var hashFilter = getHashFilter();
    if ( !hashFilter && isIsotopeInit ) {
      return;
    }
    isIsotopeInit = true;
    // filter isotope

    $container.isotope({
      filter: hashFilter,
      masonry: {
        gutter: 20
      }
    });

      if ( hashFilter ) {
        var $optionSet = $('.option-set');
        $optionSet.find('.selected').removeClass('selected');
        $('[data-filter-value="' + hashFilter + '"]').addClass('selected');
      }
    
  }

  $(window).on( 'hashchange', onHashchange );
  // trigger event handler to init Isotope
  onHashchange();
});