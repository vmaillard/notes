


document.addEventListener("DOMContentLoaded", function() {
  init();
}, false);

function init() {

  var tags = document.getElementById("tags");
  var tagsExistants = document.getElementsByClassName("tagsExistants")[0];
  var tagsP = tags.getElementsByTagName('li');
  var content = document.getElementById("content");
  if (content != undefined) {
  // Au click sur, "toutes les notices", on referme les trieurs
  var toutesLesNotices = tags.getElementsByTagName('a')[0];

  toutesLesNotices.addEventListener('click', fermeture, false);

  var a = document.getElementsByClassName('bouton');
  for (var i = 0; i < a.length; i++) {
    a[i].addEventListener('click', montreMoi, false);
  }
  }

  for (var i = 0; i < tagsP.length; i++) {
    if (argumentOntologique(tagsExistants) == undefined) {
    tagsP[i].addEventListener('click', enScène, false);
    tagsP[i].addEventListener('mouseover', survol, false);
    tagsP[i].addEventListener('mouseout', horsvol, false);
    }
  }

}



// On attrape les titres, tags, auteur, genre pour en créer des tags isotope
function creationTag(array, el) {
  for (var i = 0; i < el.length; i++) {
  if (argumentOntologique(el[i])) {
    var valeurTag = el[i].innerHTML;
    var array_valeurTag = valeurTag.split(", ");

    el[i].innerHTML = '';
    el[i].className = "filter option-set";
    el[i].setAttribute("data-filter-group", "item");
    
    for (var j in array_valeurTag) {
    var tag = document.createElement('a');
    tag.setAttribute("class", "bouton");
    tag.setAttribute("data-filter-value", "." + normalize(array_valeurTag[j]));
    tag.innerHTML = array_valeurTag[j];
    el[i].appendChild(tag);

    array.push(tag.cloneNode(true));
    }
  }
  }
}


// On normalise les titres, tags, auteur, genre pour Isotope
function normalize(el) {
  if (argumentOntologique(el)) {
    if (typeof el == 'string') {
    return el.toLowerCase().split(' ').join('-').split(',-').join(' ').replace(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, "-");
    } else {
    return el.innerHTML.toLowerCase().split(' ').join('-').split(',-').join(' ').replace(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/g, "-");
    }
  }
}

// Remove duplicate tags
function uniq(a) {
  var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];
  return a.filter(function(item) {
  var type = typeof item.innerHTML;
  if(type in prims)
      return prims[type].hasOwnProperty(item.innerHTML) ? false : (prims[type][item.innerHTML] = true);
  else
      return objs.indexOf(item.innerHTML) >= 0 ? false : objs.push(item.innerHTML);
  });
}