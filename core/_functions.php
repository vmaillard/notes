<?php

# yaml
require_once "core/vendor/autoload.php";

use Symfony\Component\Yaml\Yaml;
$yaml_parsed = Yaml::parse(file_get_contents('config/config.yml'));

$titre = $yaml_parsed['titre'];
$content = $yaml_parsed['url_pad'];
for($i = 0; $i <= 3; $i++) {
    $tags[$i] = $yaml_parsed['tags'][$i];
}


function parsePad($data) {

	# parsedown
	require_once "lib/parsedown/Parsedown.php";
	$parsedown = new Parsedown();

	# parsedown-extra
	require_once "lib/parsedown-extra/ParsedownExtra.php";
	$extra = new ParsedownExtra();

	$ParsedownExtra = new ParsedownExtra();

	# pad export
    $content = file_get_contents($data.'/export/txt');
    $content = $ParsedownExtra->text($content);

    # array balises
    $pregEnter = array();
    $pregExit = array();
    $strEnter = array();
    $strExit = array();

    # image from web (html) 
    # $pregEnter[0] = '/\[img\]<a href="(.*?)">(.*?)<\/a>\[img\]/';
    # $pregExit[0] = '<img src="$1" />';
    
    # video from web (html)
    # $pregEnter[2] = '/\[video\]<a href="(.*?)">(.*?)<\/a>\[video\]/';
    # $pregExit[2] = '<video><source src="$1">';

    # remplace array
    $content = preg_replace($pregEnter, $pregExit, $content);
	
	# characters to good characters
	$strEnter[0] = '“ ';
    $strExit[0] = '“';
    
    $strEnter[1] = ' ”';
    $strExit[1] = '”';
    
    $strEnter[2] = "'";
    $strExit[2] = '’';

    $strEnter[3] = "<h1>";
    $strExit[3] = '<div><h1>';
    
    $strEnter[4] = "</h4>";
    $strExit[4] = "</h4></div>";  	
	
	$content = str_replace($strEnter, $strExit, $content);

    $a = 1;
    $b = 2;
    $array_value = array();

    $content = preg_replace_callback(
            // on capture le contenu des div
            // regex = tous les cactères et sauts de ligne entre <div>
            "/<div>(.+?)<\/div>/s",
            "set_divs_tags",
            $content);

    return $content;
}


function set_divs_tags($matches) {
    global $array_value;
    $array_value = array();

    $modification = preg_replace_callback(
        "#<h[1-6]>(.+?)<\/h[1-6]>#",
        "set_tags",
        $matches[1]);

	$début = "<div ";
	$str = implode(" ", $array_value);
	$class = "class='" . $str . "'>"; 
	$fin = "</div>";

	$wrap = $début . $class . $modification . $fin;

	return $wrap;
}



function set_tags($matches) {
	// $matches[0] = contenu total = <h1>Métalepse</h1>
	// $matches[1] = contenu du titre = Métalepse
	// $matches[1]{0} = = M
	// $matches[2] = ?
	global $array_value;
	$array_value_temp = array();

	$niveau = $matches[0]{2};
	$début = "<h$niveau class='filter option-set' data-filter-group='item'>";
	$fin = "</h$niveau>";
        
	$total_liens = "";
	$array_str = explode(", " , $matches[1]);

	foreach($array_str as $str) {
		$value_isotope = normaliser($str);
		$lien = "<a class='bouton $value_isotope' data-filter-value='.$value_isotope'><span>$str</span></a>";
		array_push($array_value_temp, $value_isotope);
		$wrapper = $début . $lien . $fin;
		$total_liens .= $wrapper;
    }
    $array_value = array_merge($array_value, $array_value_temp);
    $tableau = array($total_liens);
    return $tableau[0];
}

function normaliser($str) {
    $str = strtolower($str);
    $str = explode(" " , $str);
    $str = implode("-", $str);
    $str = explode(",-" , $str);
    $str = implode(" ", $str);
        
    $regex = '/[#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/';
    $str = preg_replace($regex, "-", $str);

    return $str;
}


