<?php include "core/_functions.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <title><?= $titre ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 

    <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>

    <script src="assets/js/lib/jquery-2.1.3.min.js" defer></script>
    <script src="assets/js/lib/jquery.isotope.js" defer></script>

    <script src="assets/js/javascript.js" defer></script>
    <script src="assets/js/interaction.js" defer></script>
    <script src="assets/js/isotope.js" defer></script>

</head>


<body>

    <main id="content">
<?php
$content = parsePad($content);
echo $content;
?>
    </main>

    <aside>

        <div class="sticky">

        <?php include "inc/menu.php"; ?>

        <div id="tags">
            <div class="filter option-set" data-filter-group="item">
            <a class="bouton selected" data-filter-value="*">Toutes les notices</a>
            </div>
            <li id="H1"><?php echo $tags[0] ?><span>↓</span></li>
            <ul class="filter option-set" id="titres" data-filter-group="item">
            <input placeholder="Recherche" class="box" type="text" />
                <?php
                preg_match_all("/<h1 class='filter option-set' data-filter-group='item'>(.+?)<\/h1>/", $content, $matches);
                $array_temp = [];
                foreach($matches[1] as $match) {
                    $array_temp[] = $match;
                }
                $array_temp = array_unique($array_temp);
                foreach ($array_temp as $match) {
                    echo $match;
                }
                ?>
            </ul>

            <li id="H2"><?php echo $tags[1] ?><span>↓</span></li>
            <ul class="filter option-set" id="auteurs" data-filter-group="item">
            <input placeholder="Recherche" class="box" type="text" />
                <?php
                preg_match_all("/<h2 class='filter option-set' data-filter-group='item'>(.+?)<\/h2>/", $content, $matches);
                $array_temp = [];
                foreach($matches[1] as $match) {
                    $array_temp[] = $match;
                }
                $array_temp = array_unique($array_temp);
                foreach ($array_temp as $match) {
                    echo $match;
                }
                ?>
            </ul>
            
            <li id="H3"><?php echo $tags[2] ?><span>↓</span></li>
            <ul class="filter option-set" id="mots-clés" data-filter-group="item">
            <input placeholder="Recherche" class="box" type="text" />
                <?php
                preg_match_all("/<h3 class='filter option-set' data-filter-group='item'>(.+?)<\/h3>/", $content, $matches);
                $array_temp = [];
                foreach($matches[1] as $match) {
                    $array_temp[] = $match;
                }
                $array_temp = array_unique($array_temp);
                foreach ($array_temp as $match) {
                    echo $match;
                }
                ?>        
            </ul>
            
            <li id="H4"><?php echo $tags[3] ?><span>↓</span></li>
            <ul class="filter option-set" id="genres" data-filter-group="item">
            <input placeholder="Recherche" class="box" type="text" />
                <?php
                preg_match_all("/<h4 class='filter option-set' data-filter-group='item'>(.+?)<\/h4>/", $content, $matches);
                $array_temp = [];
                foreach($matches[1] as $match) {
                    $array_temp[] = $match;
                }
                $array_temp = array_unique($array_temp);
                foreach ($array_temp as $match) {
                    echo $match;
                }
                ?>        
            </ul>
        </div>

        </div>

    </aside>
        
 </body>

</html>


