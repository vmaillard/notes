<?php include "core/_functions.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <title><?= $titre ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 

    <link rel="stylesheet" href="assets/css/style.css" type="text/css"/>

    <script src="assets/js/lib/jquery-2.1.3.min.js" defer></script>
    <script src="assets/js/lib/jquery.isotope.js" defer></script>

    <script src="assets/js/javascript.js" defer></script>
    <script src="assets/js/interaction.js" defer></script>
    <script src="assets/js/isotope.js" defer></script>

</head>
  
<body>


<main>
    <iframe src="<?= $content; ?>"></iframe>
</main>

<aside>


<?php include "inc/menu.php"; ?>

    <div id="notice">

    <div class="typologie" style="display:block; width:100%;">
    <li>Écrire une notice</li>

<textarea class="js-copytextarea" content-editable="false">
# Titre  
## Auteur1 
### Mots-clés  
Description
#### Genre
</textarea>

    <button class="js-textareacopybtn">Copier</button>
    </div>

        <div class="typologie">
        <li>Italic, bold</li>
        <ul style="display:block;">
        *italic*<br>
        **bold**
        </ul>
        </div>

        <div class="typologie">
        <li>Saut de ligne</li>
        <ul style="display:block;">
        Double espace<br>
        </ul>
        </div>

        <div class="typologie">
        <li>Guillemets français</li>
        <ul style="display:block;">
        « »
        </ul>
        </div>

        <div class="typologie">
        <li>Lien</li>
        <ul style="display:block;">
        [nom du lien](url)
        </ul>
        </div>

    </div>
</aside>

</body>


</html>


